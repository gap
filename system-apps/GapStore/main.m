//
//  main.m
//  myInstaller
//
//  Created by Dr. H. Nikolaus Schaller on Sun Aug 03 2003.
//  Copyright (c) 2003 __MyCompanyName__.
//  GPL V2
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char *argv[])
{
    return NSApplicationMain(argc, argv);
}
