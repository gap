//
//  AppController.m
//  GPL V2
//

#import <Cocoa/Cocoa.h>

@interface AppController : NSObject
{
	IBOutlet NSWindow *mainWindow;
	IBOutlet NSSearchField *searchField;
	NSMutableArray *appList;
	IBOutlet NSTableView *appTable;
}

- (IBAction) refresh:(id) sender;
- (IBAction) singleClick:(id) sender;
- (IBAction) doubleClick:(id) sender;

@end
