//
// AppController.m
//  GPL V2
//

#import "AppController.h"

@implementation AppController

- (BOOL) applicationShouldTerminateAfterLastWindowClosed:(NSApplication *) app;
{
	return YES;
}

- (void) awakeFromNib
{
	[appTable setDoubleAction:@selector(doubleClick:)];
}

- (void) dealloc
{
	[appList release];
	[super dealloc];
}

- (IBAction) refresh:(id) sender;
{
	[appList release];
	appList=nil;
	[appTable reloadData];
}

- (IBAction) singleClick:(id) sender;
{
}

- (IBAction) doubleClick:(id) sender;
{
	NSInteger row=[appTable selectedRow];
	id appnum;
	NSString *urlString;
	NSURL *url;
	if(row < 0)
		return;
	appnum=[[appList objectAtIndex:row] objectForKey:@"id"];
	urlString=[NSString stringWithFormat:@"http://www.gnustep.org/softwareindex/showdetail.php?app=%ld", (long) [appnum integerValue]];
	url=[NSURL URLWithString:urlString];
	// open in web browser
	[[NSWorkspace sharedWorkspace] openURL:url];
}

- (BOOL) filter:(NSString *) filter value:(NSString *) value
{ // return YES if value should be included
	if(!value)
		return NO;	// ignore
	return [value rangeOfString:filter].location != NSNotFound;	// could be case insensitive
}

- (int) numberOfRowsInTableView:(NSTableView *)aTableView
{
	if(!appList)
		{
		appList=[[NSMutableArray arrayWithCapacity:100] retain];
		NSMutableData *d=[NSMutableData dataWithContentsOfURL:[NSURL URLWithString:@"http://www.gnustep.org/softwareindex/plist.php"]];
		// liefert nil - evtl. pr�fe Apple auf Apple-DTD?
		NSString *error;
		NSArray *apps=[NSPropertyListSerialization propertyListFromData:d mutabilityOption:NSPropertyListImmutable format:NULL errorDescription:&error];
		/* This is a workaround for a bug on the server side (SWI)
		 *
		 * the server sends
		 * <?xml version="1.0" encoding="UTF-8"?>
		 * but the string encoding is "ISO-8859-1"
		 * which can not be properly processed
		 *
		 * The hack is to replace the "UTF-8" with "ISO-8859-1"
		 * and try again
		 */
		if(!apps && [error isEqualToString:@"Unable to convert string to correct encoding"])
			{
			char *iso="ISO-8859-1";
			[d replaceBytesInRange:NSMakeRange(30, 5) withBytes:iso length:strlen(iso)];
			apps=[NSPropertyListSerialization propertyListFromData:d mutabilityOption:NSPropertyListImmutable format:NULL errorDescription:&error];
			}
		NSEnumerator *e=[apps objectEnumerator];
		NSDictionary *app;
		NSString *filter=[[searchField stringValue] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		while((app=[e nextObject]))
			{
			if([filter length])
				{ // apply full-text search filter
				BOOL include = [self filter:filter value:[app objectForKey:@"name"]];
				include |= [self filter:filter value:[app objectForKey:@"summary"]];
				include |= [self filter:filter value:[app objectForKey:@"description"]];
				include |= [self filter:filter value:[app objectForKey:@"license"]];
				include |= [self filter:filter value:[app objectForKey:@"author"]];
				include |= [self filter:filter value:[app objectForKey:@"category"]];
				if(!include)
					continue;
				}

			// more filters:
			// a) newest version only (check name to already exist and substitute if newer so that we only show the latest version)
			// b) check and skip what is already installed

			[appList addObject:app];
			}
		[appList sortUsingDescriptors:[appTable sortDescriptors]];
		}
	return [appList count];
}

- (void) tableView:(NSTableView *) tableView sortDescriptorsDidChange:(NSArray *) oldDescriptors;
{
	[appList sortUsingDescriptors:[tableView sortDescriptors]];
	[tableView reloadData];
}

- (id) tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
	NSString *ident=[aTableColumn identifier];
	// FIXME: show icon
	return [[appList objectAtIndex:rowIndex] objectForKey:ident];
}

/*
- (BOOL) validateMenuItem:(id <NSMenuItem>) menuItem;
{
	NSString *cmd=NSStringFromSelector([(NSMenuItem *) menuItem action]);
	// FIXME:
	if([cmd isEqualToString:@"format:"])
		return NO;
	return YES;
}
*/

@end
